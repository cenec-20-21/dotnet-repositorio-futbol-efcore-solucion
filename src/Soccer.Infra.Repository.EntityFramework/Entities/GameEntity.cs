﻿using System;
using System.Collections.Generic;

namespace Soccer.Infra.Repository.EntityFramework.Entities
{
    public class GameEntity
    {
        public int Id { get; set; }
        public Guid GameGuid { get; set; }
        public int LocalTeamId { get; set; }
        public TeamEntity LocalTeam { get; set; }
        public TeamEntity ForeignTeam { get; set; }
        public int ForeignTeamId { get; set; }
        public List<GoalEntity> Goals { get; set; }
        public DateTime? StartedOn { get; set; }
        public DateTime? EndedOn { get; set; }
    }
}