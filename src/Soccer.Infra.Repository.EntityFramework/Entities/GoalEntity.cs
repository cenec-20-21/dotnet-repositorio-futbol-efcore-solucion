﻿using System;

namespace Soccer.Infra.Repository.EntityFramework.Entities
{
    public class GoalEntity
    {
        public int Id { get; set; }
        public string ScoredBy { get; set; }
        public DateTime ScoredOn { get; set; }
        public GameEntity Game { get; set; }
        public TeamEntity Team { get; set; }
        public int TeamId { get; set; }
    }
}